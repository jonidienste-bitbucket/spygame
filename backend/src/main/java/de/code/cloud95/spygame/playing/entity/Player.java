package de.code.cloud95.spygame.playing.entity;

import javax.persistence.*;

@Entity
@NamedQuery(name = Player.BY_GAME_ID, query = "select p from Player p where p.gameId = :gameId")
public class Player{
    public static final String BY_GAME_ID = "Player.byGameId";
    @Id
    @GeneratedValue
    public Long id;
    public String name;
    public Long gameId;
    @Embedded
    private CardId cardId;

    /**
     * Constructors
     */
    public Player() {}
    public Player(String name){
        this.name = name;
    }
    public Player(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    /**
     * Queries
     */
    public static final String PLAYER = Player.class.getSimpleName();
    public static final String ALL = "from "+ PLAYER;
    public static final String WAITING = "from "+ PLAYER + " where gameId = null";


    public CardId cardId() {
        if(cardId == null){
            throw new RuntimeException("Player \""+name+"\" does not have a card.");
        }
        return cardId;
    }

    public void cardId(CardId cardId) {
        this.cardId = cardId;
    }
}
