package de.code.cloud95.spygame.page.entity;

import de.code.cloud95.spygame.page.boundary.Page;

public class Form implements Renderable{
    private final Renderable[] renderables;
    private final String action;

    public <T extends Page> Form(Class<T> page, Renderable...renderables){
        action = page.getSimpleName();
        this.renderables = renderables;
    }

    @Override
    public String render() {
        return "<form action=\""+action+"\" method=\"get\">"+render(renderables)+"</form>";
    }
}
