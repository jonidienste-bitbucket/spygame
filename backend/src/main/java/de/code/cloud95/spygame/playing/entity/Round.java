package de.code.cloud95.spygame.playing.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@NoArgsConstructor
public class Round {
    @Id
    @GeneratedValue
    @Getter
    private Long id;
    @Getter
    private Long deckId;
    @ManyToOne
    private Game game;

    public void setDeck(Long deckId){
        this.deckId = deckId;
    }
}
