package de.code.cloud95.spygame.authentication.control;

import io.smallrye.jwt.build.Jwt;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonNumber;
import java.util.HashSet;
import java.util.List;

@RequestScoped
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class TokenGenerator {
    final JsonWebToken jwt;

    public static final String PLAYER_ID = "playerId";

    public String generateToken(String name, long id) {
        if(id == 0L){
            throw new RuntimeException("Invalid player id 0L");
        }
        String token =
                Jwt.issuer("https://example.com/issuer")
                        .upn(name)
                        .groups(new HashSet<>(List.of("player")))
                        .claim(PLAYER_ID, id)
                        .sign();
        return token;
    }

    public Long getPlayerId(){
        System.out.println("hiiii"+jwt.getClaimNames());
        JsonNumber claim = jwt.getClaim(PLAYER_ID);
        System.out.println(claim);
        return claim.longValue();
    }
}
