package de.code.cloud95.spygame.page.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Text implements Renderable {
    private final String text;
    @Override
    public String render() {
        return text;
    }
}
