package de.code.cloud95.spygame.authentication.boundary;

import de.code.cloud95.spygame.authentication.control.UserService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@SecuritySchemes(value = {
        @SecurityScheme(securitySchemeName = "apiKey",
                type = SecuritySchemeType.HTTP,
                scheme = "Bearer")}
)
@Path("user")
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class UserResource {
    public static final java.lang.String API_KEY = "apiKey";
    final UserService us;

    @SecurityRequirement(name = API_KEY)
    @GET
    public JsonObject getUser(){
        if(us.getUser() == null){
            throw new RuntimeException("UserNotLoggedIn");
        }
        return Json.createObjectBuilder()
                .add("name", us.getUser().name)
                .add("id", us.getUser().id)
                .build();
    }

}
