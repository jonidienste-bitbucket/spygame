package de.code.cloud95.spygame.stopwatch.entity;

import javax.enterprise.context.RequestScoped;
import java.time.LocalDateTime;

@RequestScoped
public class Clock {
    public LocalDateTime now(){
        return LocalDateTime.now();
    }
}
