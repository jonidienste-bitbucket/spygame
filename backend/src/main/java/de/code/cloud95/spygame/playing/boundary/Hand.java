package de.code.cloud95.spygame.playing.boundary;

import de.code.cloud95.spygame.StatelessBoundary;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Card;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;

@StatelessBoundary
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class Hand {

    final UserService userService;
    final Decks decks;

    public Card viewCard() {
        var card = decks.findCardById(userService.getUser().cardId());
        return card;
    }
}
