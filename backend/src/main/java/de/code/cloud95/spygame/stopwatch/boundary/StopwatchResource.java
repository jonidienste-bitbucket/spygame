package de.code.cloud95.spygame.stopwatch.boundary;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.time.Duration;

@Path("stopwatch")
public class StopwatchResource {
    @Inject
    Stopwatch stopwatch;

    @GET
    public Duration getRemainingTime(){
        return stopwatch.remainingTime();
    }
    @POST
    public void startStop(){
        stopwatch.startStop();
    }
}
