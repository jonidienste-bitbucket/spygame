package de.code.cloud95.spygame.playing.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class Card {

    public CardId cardId;
    public Location location;
    public String job;

    public static final String AGENT = "Agent";

    public boolean isAgent() {
        return this.job.equals(AGENT);
    }
}
