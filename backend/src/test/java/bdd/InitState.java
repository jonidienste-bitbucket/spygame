package bdd;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.AfterStage;
import com.tngtech.jgiven.annotation.BeforeStage;
import com.tngtech.jgiven.annotation.ProvidedScenarioState;
import com.tngtech.jgiven.annotation.ScenarioState;
import de.code.cloud95.spygame.authentication.control.TokenGenerator;
import iteractor.lib.TestEntityManager;

import javax.persistence.EntityManager;

public class InitState<SELF extends Stage<SELF>> extends Stage<SELF> {

    @ScenarioState
    protected EntityManager em;
    @ProvidedScenarioState
    TokenGenerator tokenGenerator;

    @BeforeStage
    void begiasan(){
        em = TestEntityManager.create();
        em.getTransaction().begin();
    }

    @AfterStage
    void commiasat(){
        em.getTransaction().commit();
    }
}
