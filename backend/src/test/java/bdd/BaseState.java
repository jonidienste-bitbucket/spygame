package bdd;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.AfterStage;
import com.tngtech.jgiven.annotation.BeforeStage;
import com.tngtech.jgiven.annotation.ProvidedScenarioState;
import com.tngtech.jgiven.annotation.ScenarioState;
import de.code.cloud95.spygame.authentication.control.TokenGenerator;
import de.code.cloud95.spygame.playing.entity.Player;
import iteractor.lib.TestEntityManager;
import org.mockito.Mockito;

import javax.persistence.EntityManager;

public class BaseState<SELF extends Stage<SELF>> extends Stage<SELF> {

    @ScenarioState
    protected EntityManager em;

    @ScenarioState
    protected TokenGenerator tokenGenerator;

    @BeforeStage
    void begin(){
        em.getTransaction().begin();
    }

    @AfterStage
    void commit(){
        em.getTransaction().commit();
    }

    public SELF player(String name){
        Mockito.when(tokenGenerator.getPlayerId()).thenReturn(em.createQuery("from Player p where p.name like ?1", Player.class).setParameter(1,name).getSingleResult().id);
        return (SELF) this;
    }
}
