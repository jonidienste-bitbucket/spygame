package bdd;

import com.tngtech.jgiven.annotation.BeforeStage;
import com.tngtech.jgiven.annotation.ExpectedScenarioState;
import com.tngtech.jgiven.annotation.ScenarioState;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import de.code.cloud95.spygame.playing.boundary.Decks;
import de.code.cloud95.spygame.playing.boundary.PlayerIsNotStartplayerException;
import de.code.cloud95.spygame.playing.control.RoundService;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LobbyOutcome extends BaseState<LobbyOutcome> {
    @ExpectedScenarioState
    Lobby lobby;

    @ScenarioState
    Decks decks;

    @ExpectedScenarioState
    UserService userService;

    @ScenarioState
    RoundService roundService;

    @BeforeStage
    void initDecksAndRoundService(){
        decks = new Decks(userService, em);
        roundService = new RoundService(userService,em);
    }

    public LobbyOutcome number_of_waiting_players_is(int i) {
        assertEquals(i, lobby.waiting().size());
        return this;
    }

    public void the_first_waiting_player_is(String name) {
        assertEquals(name, lobby.waiting().get(0).name);
    }

    public void current_player_name_is(String name) {
        assertNotNull(userService.getUser());
        assertEquals(name,userService.getUser().name);
    }

    public void can_not_view_decks() {
        Assertions.assertThrows(PlayerIsNotStartplayerException.class, ()-> decks.list());
    }

    public void can_view_all_decks() {
        assertEquals(2,decks.list().size());
    }

    public LobbyOutcome number_of_active_games_is(int i) {
        var rounds = roundService.findAll();
        assertEquals(i, rounds.size());
        return this;
    }

    public void first_game_has_deck(Long deckId) {
        var round = roundService.findAll().get(0);
        assertNotNull(deckId);
        assertEquals(deckId,round.getDeckId());
    }

}
