package de.code.cloud95.spygame.business.playing.entity;

import de.code.cloud95.spygame.playing.entity.Player;
import iteractor.lib.TestEntityManager;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    EntityManager em = TestEntityManager.create();
    @Test
    void save(){
        em.getTransaction().begin();
        em.merge(new Player());
        em.getTransaction().commit();
        assertEquals(1,em.createQuery("from Player").getResultList().size());
    }

    @Test
    void byGameId(){
        em.getTransaction().begin();
        var p = new Player();
        p.gameId = 12L;
        p.name = "Max";
        em.merge(p);
        em.getTransaction().commit();
        em.getTransaction().begin();
        Player player = em.createNamedQuery(Player.BY_GAME_ID, Player.class).setParameter("gameId", 12L).getSingleResult();
        assertEquals("Max", player.name);
    }
}