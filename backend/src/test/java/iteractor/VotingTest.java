package iteractor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class VotingTest {
/*
    AppFactory app;
    long agent;
    long player;
    long player2;
    LocalDateTime time = LocalDateTime.now();

    @BeforeEach
    void init(){
        app = new AppFactory(() -> time, deck -> {
            if(!deck.cards().get(0).isAgent()){
                throw new RuntimeException("For tests: First card of deck should be the agent card. No shuffeling for repoducable tests");
            }
            return deck;
        });
        agent = app.user().run(JoinLobby.class, new JoinLobbyRequest("Max Agent")).playerId();
        player = app.user().run(JoinLobby.class, new JoinLobbyRequest("Peter Player")).playerId();
        player2 = app.user().run(JoinLobby.class, new JoinLobbyRequest("Tim Player2")).playerId();
        app.user(player).run(StartGame.class, new StartGameRequest(3,5));
        app.user(player).run(ChooseCardDeck.class, new ChooseCardDeckRequest(1L));
    }

    //@Disabled
    @Test
    void should_allowVoting_when_roundIsActive(){
        assertTrue(app.user(agent).run(LoadRoundState.class).canRequestVoting());
        assertTrue(app.user(player).run(LoadRoundState.class).canRequestVoting());
    }
    @Test
    void should_not_showLocationGuessing_when_roundIsActive(){
        assertFalse(app.user(agent).run(LoadRoundState.class).showLocationGuessingInput());
    }
    @Disabled
    @Test
    void should_showLocationGuessing_when_agentRequestsVoting(){
        app.user(agent).run(RequestVoting.class);
        assertTrue(app.user(agent).run(LoadRoundState.class).showLocationGuessingInput());
    }
    @Disabled
    @Test
    void should_showPlayerVoting_when_playerRequestsVoting(){
        app.user(agent).run(RequestVoting.class);
        assertTrue(app.user(agent).run(LoadRoundState.class).showPlayerVotingInput());
    }
/*
    @Test
    void should_beInLocationGuessing_when_agentRequestsVoting(){
        var before = app.user(agent).run(LoadRoundState.class);
        assertTrue(before.canRequestVoting());
        assertFalse(before.showLocationGuessingInput());

        app.user(agent).run(RequestVoting.class);

        var after = app.user(agent).run(LoadRoundState.class);
        assertFalse(after.canRequestVoting());
        assertTrue(after.showLocationGuessingInput());
        var otherPlayers = app.user(player).run(LoadRoundState.class);
        assertFalse(otherPlayers.canRequestVoting());
        assertFalse(otherPlayers.showLocationGuessingInput());
    }

    @Test
    void should_beInPlayerVoting_when_playerRequestsVoting(){
        var before = app.user(player).run(LoadRoundState.class);
        assertTrue(before.canRequestVoting());
        assertFalse(before.showPlayerVotingInput());

        app.user(agent).run(RequestVoting.class);

        var after = app.user(player).run(LoadRoundState.class);
        assertFalse(after.canRequestVoting());
        assertTrue(after.showPlayerVotingInput());
        var otherPlayers = app.user(player2).run(LoadRoundState.class);
        assertFalse(otherPlayers.canRequestVoting());
        assertTrue(otherPlayers.showPlayerVotingInput());
    }


    @Test
    void should_beInVotingMode_when_timerExpired(){
        var before = app.user(player).run(LoadRoundState.class);
        assertTrue(before.canRequestVoting());
        assertFalse(before.showPlayerVotingInput());
        assertFalse(before.showLocationGuessingInput());
        var beforeAgent = app.user(agent).run(LoadRoundState.class);
        assertTrue(beforeAgent.canRequestVoting());
        assertFalse(beforeAgent.showPlayerVotingInput());
        assertFalse(beforeAgent.showLocationGuessingInput());

        time = time.plusMinutes(10);
        var after = app.user(player).run(LoadRoundState.class);
        assertFalse(after.canRequestVoting());
        assertTrue(after.showPlayerVotingInput());
        assertFalse(after.showLocationGuessingInput());
        var afterAgent = app.user(agent).run(LoadRoundState.class);
        assertFalse(afterAgent.canRequestVoting());
        assertTrue(afterAgent.showPlayerVotingInput());
        assertFalse(afterAgent.showLocationGuessingInput());
    }
 */
}
