package iteractor;



import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.boundary.Decks;
import de.code.cloud95.spygame.playing.boundary.Startplayer;
import de.code.cloud95.spygame.playing.entity.Player;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import de.code.cloud95.spygame.stopwatch.boundary.Stopwatch;
import de.code.cloud95.spygame.stopwatch.entity.Clock;
import iteractor.lib.TestEntityManager;
import iteractor.lib.TestUserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TimerTest {

    Lobby lobby;
    Stopwatch stopwatch;
    Startplayer startplayer;
    UserService userService;

    EntityManager em;

    Player player1;
    Player player2;
    Player currentPlayer;

    Clock clock;

    @BeforeEach
    void init(){
        em = TestEntityManager.create();
        userService = new TestUserService(em,()->currentPlayer);
        lobby = new Lobby(userService, em);
        var decks = new Decks(userService, em);
        startplayer = new Startplayer(userService, em, lobby, decks);

        clock = mock(Clock.class);
        stopwatch = new Stopwatch(userService,em, clock);
        em.getTransaction().begin();
    }

    private Player addPlayer(String name) {
        userService.addUser(name);
        var all = em.createQuery("from Player", Player.class).getResultList();
        return all.get(all.size()-1);
    }

    @AfterEach
    void commit(){
        em.getTransaction().commit();
    }

    @Test
    void should_haveRemainingTime_when_stopwatchIsRunning(){
        LocalDateTime time = LocalDateTime.now();
        when(this.clock.now()).thenReturn(time);

        player1 = addPlayer("Joni");
        player2 = addPlayer("max");

        currentPlayer = player1;
        startplayer.startNewGame(1,10);

        stopwatch.startStop();

        time = time.plusMinutes(8);
        when(this.clock.now()).thenReturn(time);

        currentPlayer = player1;
        assertEquals(Duration.ofMinutes(2), stopwatch.remainingTime());
        currentPlayer = player2;
        assertEquals(Duration.ofMinutes(2), stopwatch.remainingTime());
    }

}
