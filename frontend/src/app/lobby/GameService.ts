import RestClient from "../AppUtil";
import {Player} from "../generated/api/model/player";

export default class LobbyService {

    restClient:RestClient;

    constructor(restClient:RestClient){
        this.restClient = restClient;
    }

    public isAuthenticated():boolean{
       return this.restClient.hasToken();
    }

}