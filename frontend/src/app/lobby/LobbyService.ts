import RestClient from "../AppUtil";
import {Player} from "../generated/api/model/player";
import {StartGameRequest} from "../generated/api/model/startGameRequest";

export default class LobbyService {

    restClient:RestClient;

    constructor(restClient:RestClient){
        this.restClient = restClient;
    }

    async loadPlayers():Promise<Player[]>{
       return this.restClient.get("/lobby");
    }

    async join(name) {
        let result = await this.restClient.put("/lobby/join", {name:name});
        await this.restClient.setToken(result.token);
    }

    async startGame(req:StartGameRequest) {
        await this.restClient.post("/lobby/start", req);
    }
}