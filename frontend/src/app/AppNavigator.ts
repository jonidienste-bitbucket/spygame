import {AppNavigatorBase} from "./AppUtil";

export class AppNavigator extends AppNavigatorBase{
    public lobby = () => this.navigate("lobby");
    public isLobby = () => this.isPage("lobby");
    public playing = () => this.navigate("playing");
    public isPlaying = () => this.isPage("playing");
}

export const appNavigator = new AppNavigator();